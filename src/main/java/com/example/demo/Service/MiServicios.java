/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.Service;

import org.springframework.stereotype.Component;


/**
 *
 * @author jorda
 */
@Component("ServicioSimple")
public class MiServicios implements IServicio{


    @Override
    public String Operacion() {
        return "Algun proceso importante SIMPLE ... ";
    }
}
