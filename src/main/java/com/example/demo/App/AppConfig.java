/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.App;

import com.example.demo.Beans.ItemFactura;
import com.example.demo.Beans.Productos;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author jorda
 */
@Configuration
public class AppConfig {

    @Bean("ListItemFactura")
    public List<ItemFactura> registrarItems() {
        Productos producto1 = new Productos("Camara Sony", 100);
        Productos producto2 = new Productos("Bicicleta Honda aro 26", 200);
        Productos producto3 = new Productos("Alicate rojo", 17.5);
        Productos producto4 = new Productos("Cemento Sol del Norte", 135.90);

        ItemFactura ItemFactura1 = new ItemFactura(producto1, 2);
        ItemFactura ItemFactura2 = new ItemFactura(producto2, 3);
        ItemFactura ItemFactura3 = new ItemFactura(producto3, 4);
        ItemFactura ItemFactura4 = new ItemFactura(producto4, 5);

        List<ItemFactura> LItemFac = new ArrayList<>();
        LItemFac.add(ItemFactura1);
        LItemFac.add(ItemFactura2);
        LItemFac.add(ItemFactura3);
        LItemFac.add(ItemFactura4);

        return LItemFac;
        
    }
    
    @Bean("ListItemFacturaOficina")
    public List<ItemFactura> registrarItems3() {
    Productos producto1 = new Productos("Notbook ASUS", 500);
        Productos producto2 = new Productos("Monitor LG LCD 24", 200);
        Productos producto3 = new Productos("Impresora HP multifuncional", 80.9);
        Productos producto4 = new Productos("Escritorio de oficina", 635.90);

        ItemFactura ItemFactura1 = new ItemFactura(producto1, 1);
        ItemFactura ItemFactura2 = new ItemFactura(producto2, 3);
        ItemFactura ItemFactura3 = new ItemFactura(producto3, 1);
        ItemFactura ItemFactura4 = new ItemFactura(producto4, 1);

        List<ItemFactura> LItemFacOficina = new ArrayList<>();
        LItemFacOficina.add(ItemFactura1);
        LItemFacOficina.add(ItemFactura2);
        LItemFacOficina.add(ItemFactura3);
        LItemFacOficina.add(ItemFactura4);

        return LItemFacOficina;
    }
}
