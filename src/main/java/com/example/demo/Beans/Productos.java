/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.Beans;

/**
 *
 * @author jorda
 */
public class Productos {

//    private int Conteo;
//    private String Codigo;
    private String Nombre;
//    private int Cantidad;
    private double Precio;
//
//    public Productos() {
//    }
//
//    public Productos(int Conteo, String Codigo, String Nombre, int Cantidad, double Precio) {
//        this.Conteo = Conteo;
//        this.Codigo = Codigo;
//        this.Nombre = Nombre;
//        this.Cantidad = Cantidad;
//        this.Precio = Precio;
//    }
//
//    public int getConteo() {
//        return Conteo;
//    }
//
//    public void setConteo(int Conteo) {
//        this.Conteo = Conteo;
//    }
//
//    public String getCodigo() {
//        return Codigo;
//    }
//
//    public void setCodigo(String Codigo) {
//        this.Codigo = Codigo;
//    }
//
//    public String getNombre() {
//        return Nombre;
//    }
//
//    public void setNombre(String Nombre) {
//        this.Nombre = Nombre;
//    }
//
//    public int getCantidad() {
//        return Cantidad;
//    }
//
//    public void setCantidad(int Cantidad) {
//        this.Cantidad = Cantidad;
//    }
//
//    public double getPrecio() {
//        return Precio;
//    }
//
//    public void setPrecio(double Precio) {
//        this.Precio = Precio;
//    }
//
//    @Override
//    public String toString() {
//        return "Productos{" + "Conteo=" + Conteo + ", Codigo=" + Codigo + ", Nombre=" + Nombre + ", Cantidad=" + Cantidad + ", Precio=" + Precio + '}';
//    }
//    

    public Productos(String Nombre, double Precio) {
        this.Nombre = Nombre;
        this.Precio = Precio;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public double getPrecio() {
        return Precio;
    }

    public void setPrecio(double Precio) {
        this.Precio = Precio;
    }

}
