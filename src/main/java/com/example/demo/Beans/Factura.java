/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.Beans;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author jorda
 */
@Component
public class Factura {
    @Value("${texto.factura.Descripcion}")
    private String Descripcion;
    @Autowired
    private Cliente cliente;
    @Autowired
    @Qualifier("ListItemFacturaOficina")
    private List<ItemFactura> ItemFactura;

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<ItemFactura> getItemFactura() {
        return ItemFactura;
    }

    public void setItemFactura(List<ItemFactura> ItemFactura) {
        this.ItemFactura = ItemFactura;
    }
}
