/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.Beans;

/**
 *
 * @author jorda
 */
public class ItemFactura {
    private Productos Producto;
    private int Cantidad;

    public ItemFactura(Productos Producto, int Cantidad) {
        this.Producto = Producto;
        this.Cantidad = Cantidad;
    }

    public Productos getProducto() {
        return Producto;
    }

    public void setProducto(Productos Producto) {
        this.Producto = Producto;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int Cantidad) {
        this.Cantidad = Cantidad;
    }
    
    public double getImporte(){
        return Cantidad* this.Producto.getPrecio();
    }
}
