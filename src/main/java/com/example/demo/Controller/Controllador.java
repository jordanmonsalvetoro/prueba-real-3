package com.example.demo.Controller;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */


import com.example.demo.Beans.Productos;
import com.example.demo.Service.IServicio;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author jorda
 */
@Controller
public class Controllador {
   
    
    @Value("${texto.properties.Dato.Producto.Conteo}")
    private int Conteo;
    @Value("${texto.properties.Dato.Producto.Codigo}")
    private String Codigo;
    @Value("${texto.properties.Dato.Producto.Nombre}")
    private String Nombre;
    @Value("${texto.properties.Dato.Producto.Cantidad}")
    private int Cantidad;
    @Value("${texto.properties.Dato.Producto.Precio}")
    private double Precio;
    
    @GetMapping("/inicio")
    public String Inicio( HttpServletRequest request , Model model){
        List<Productos> LProducto=new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            LProducto.add(new Productos(Nombre+i, Precio+i));
        }
        model.addAttribute("ListaProductos", LProducto);
        return "inicio";
    }
    @GetMapping("/hola")
    public String Inicio(Model model) {
        model.addAttribute("mensaje", "CORECTO ...");
        return "hola";
    }
    @Autowired
    @Qualifier("ServicioComplejo")
    private IServicio miServicio;
    
    @Autowired
    @Qualifier("ServicioSimple")
    private IServicio miServicio2;
    @GetMapping("/Component")
    public String MiComponent(Model model){
        model.addAttribute("Miservicio", miServicio.Operacion()+" / "+miServicio2.Operacion());
        return "/Miservicio/MiService";
    }
}
