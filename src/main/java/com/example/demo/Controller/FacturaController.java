/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.Controller;

import com.example.demo.Beans.Factura;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author jorda
 */
@Controller
@RequestMapping("/factura")
public class FacturaController {
    @Autowired
    private Factura factura;
    @GetMapping("/ver")
    public String Index(Model model){
        model.addAttribute("titulo", "EJEMPLO FSCTURA CON INYECCION DE DEPENDENCIA");
        model.addAttribute("factura", factura);
        return "factura/ver";
    }
}
